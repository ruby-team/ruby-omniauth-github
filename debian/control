Source: ruby-omniauth-github
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Cédric Boutillier <boutil@debian.org>
Build-Depends: debhelper-compat (= 13),
               gem2deb,
               ruby-omniauth (>= 2.0~),
               ruby-omniauth-oauth2 (>= 1.8~),
               ruby-rack-test,
               ruby-rspec,
               ruby-webmock
Standards-Version: 4.6.1
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-omniauth-github.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-omniauth-github
Homepage: https://github.com/intridea/omniauth-github
Testsuite: autopkgtest-pkg-ruby
XS-Ruby-Versions: all
Rules-Requires-Root: no

Package: ruby-omniauth-github
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ${ruby:Depends},
         ${misc:Depends},
         ${shlibs:Depends}
Description: GitHub strategy for the Ruby OmniAuth library
 OmniAuth is a Ruby library that standardizes multi-provider
 authentication for web applications. It was created to be powerful,
 flexible, and do as little as possible. Any developer can create
 strategies for OmniAuth that can authenticate users via disparate
 systems. OmniAuth strategies have been created for everything from
 Facebook to LDAP.
 .
 This package contains the official OmniAuth strategy for GitHub.
